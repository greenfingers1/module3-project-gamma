from fastapi.testclient import TestClient
from main import app
from queries.plant import PlantRepository

client = TestClient(app)


class EmptyPlantQueries(PlantRepository):
    def get_all(self):
        return []


class CreatePlantQueries(PlantRepository):
    def create(self, plant):
        result = {
            "id": 1,
        }
        result.update(plant)
        return result


def test_create_plant():
    app.dependency_overrides[PlantRepository] = CreatePlantQueries

    json = {
        "name": "Rose",
        "image": "rose.png",
        "price": 5.89,
    }

    expected = {
        "id": 1,
        "name": "Rose",
        "image": "rose.png",
        "price": 5.89,
    }

    response = client.post("api/plant", json=json)

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == expected
