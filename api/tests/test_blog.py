from fastapi.testclient import TestClient
from main import app
from queries.blog import BlogQueries

client = TestClient(app)


class EmptyBlogQueries(BlogQueries):
    def get_all(self):
        return []


class CreateBlogQueries(BlogQueries):
    def create(self, blog):
        result = {
            "id": 1,
            "author_id": 1,
            "created_on": "2023-07-26T15:57:54.293004",
        }
        result.update(blog)
        return result


def test_get_all_blogs():
    app.dependency_overrides[BlogQueries] = EmptyBlogQueries

    response = client.get("/blogs")

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == []


def test_create_blog():
    app.dependency_overrides[BlogQueries] = CreateBlogQueries

    json = {
        "title": "Eggplant",
        "body": "Eggplants are nice",
        "image": "LEAVE EMPTY IF USING PEXELS",
        "author_id": 1,
    }

    expected = {
        "id": 1,
        "title": "Eggplant",
        "body": "Eggplants are nice",
        "image": "LEAVE EMPTY IF USING PEXELS",
        "created_on": "2023-07-26T15:57:54.293004",
        "author_id": 1,
    }

    response = client.post("/blogs", json=json)

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == expected
