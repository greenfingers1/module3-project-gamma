from fastapi.testclient import TestClient
from main import app
from queries.catalog_gallery import CatalogRepository


client = TestClient(app)


class EmptyCatalogRepo():
    def get_all(self):
        return []


def test_get_supplies():
    app.dependency_overrides[CatalogRepository] = EmptyCatalogRepo
    response = client.get("/catalog/supplies")
    app.dependency_overrides = {}
    assert response.status_code == 200
    assert response.json() == []
