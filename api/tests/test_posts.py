from unittest.mock import patch
from fastapi.testclient import TestClient
from main import app
from queries.competition_posts import PostOut

client = TestClient(app)


def test_create_post():
    mock_post = PostOut(
        id=1,
        user_name="JohnDoe",
        picture="post_picture.png",
        description="This is a test post.",
        created_on="2023-07-27T12:00:00",
    )
    expected_response = {
        "id": 1,
        "user_name": "JohnDoe",
        "picture": "post_picture.png",
        "description": "This is a test post.",
        "created_on": "2023-07-27T12:00:00",
    }
    post_data = {
        "user_name": "JohnDoe",
        "picture": "post_picture.png",
        "description": "This is a test post.",
    }
    competition_id = 1

    with patch(
        "queries.competition_posts.PostQueries.create", return_value=mock_post
    ):
        response = client.post(
            f"/competition/{competition_id}/posts", json=post_data
        )

        assert response.status_code == 200
        assert response.json() == expected_response
