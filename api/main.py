from fastapi import FastAPI
from routers import (
    blogs,
    cart,
    plant,
    accounts,
    catalog_gallery,
    competition_posts,
)

from fastapi.middleware.cors import CORSMiddleware
from authenticator import authenticator


app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(accounts.router)
app.include_router(authenticator.router)
app.include_router(accounts.router)
app.include_router(blogs.router)
app.include_router(cart.router)
app.include_router(plant.router)
app.include_router(catalog_gallery.router)
app.include_router(competition_posts.router)


@app.get("/")
def root():
    return {"message": "You hit the root path!"}
