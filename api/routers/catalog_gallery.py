from fastapi import APIRouter, Depends, Response
from typing import Union, List
from queries.catalog_gallery import (
    Error,
    CatalogIn,
    CatalogOut,
    CatalogRepository,
)

router = APIRouter()


@router.post("/catalog/supplies", response_model=Union[CatalogOut, Error])
def create_catalog(
    catalog: CatalogIn, response: Response, repo: CatalogRepository = Depends()
):
    return repo.create(catalog)


@router.get("/catalog/supplies", response_model=Union[Error, List[CatalogOut]])
def get_all(
    repo: CatalogRepository = Depends(),
):
    return repo.get_all()
