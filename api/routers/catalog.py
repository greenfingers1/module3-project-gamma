from fastapi import APIRouter, Response, Depends
from queries.catalog import PlantQueries

router = APIRouter()


@router.get("/api/catalog")
def get_plants(
    response: Response,
    plant: PlantQueries = Depends(),
):
    try:
        return plant.get_all()
    except Exception:
        response.status_code = 400
        return {"message": "Could not retrieve plants."}
