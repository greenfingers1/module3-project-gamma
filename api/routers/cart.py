from fastapi import APIRouter, Depends, Response
from typing import List, Union
from queries.cart import Error, CartIn, CartRepository, CartOut

router = APIRouter()


@router.post("/api/cart", response_model=Union[CartOut, Error])
def add_to_cart(
    cart: CartIn,
    response: Response,
    repo: CartRepository = Depends(),
):
    try:
        return repo.create(cart)
    except Exception:
        response.status_code = 404
        return {"message": "Cannot create new item for cart"}


@router.get("/api/cart", response_model=Union[Error, List[CartOut]])
def get_all_cart(
    repo: CartRepository = Depends(),
):
    return repo.get_all_cart()


@router.delete("/api/cart/{plant_id}", response_model=bool)
def delete_item_in_cart(
    plant_id: int,
    repo: CartRepository = Depends(),
) -> bool:
    return repo.delete(plant_id)


@router.put("/api/cart/{plant_id}", response_model=Union[List[CartOut], Error])
def update_cart(
    plant_id: int,
    cart: CartIn,
    repo: CartRepository = Depends(),
) -> Union[Error, CartOut]:
    return repo.update(plant_id, cart)
