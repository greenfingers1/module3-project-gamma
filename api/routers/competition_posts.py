from fastapi import APIRouter, HTTPException, FastAPI
from typing import List
from queries.competition_posts import PostQueries, PostIn, PostOut

app = FastAPI(debug=True)
router = APIRouter()
post_queries = PostQueries()


@router.get("/competition/{id}/posts", response_model=List[PostOut])
def get_posts(id: int):
    try:
        posts = post_queries.get_all(id)
        return posts
    except Exception:
        raise HTTPException(
            status_code=500, detail="Could not retrieve posts."
        )


@router.post("/competition/{id}/posts", response_model=PostOut)
def create_post(id: int, post_data: PostIn):
    try:
        post = post_queries.create(id, post_data)
        return post
    except Exception:
        raise HTTPException(status_code=500, detail="Could not create post.")


@router.put("/competition/{id}/posts/{post_id}", response_model=PostOut)
def update_post(id: int, post_id: int, post_data: PostIn):
    try:
        post = post_queries.update(id, post_id, post_data)
        return post
    except Exception:
        raise HTTPException(status_code=500, detail="Could not update post.")


@router.delete("/competition/{id}/posts/{post_id}", response_model=dict)
def delete_post(id: int, post_id: int):
    try:
        post_queries.delete(id, post_id)
        return {"message": "Post deleted successfully"}
    except Exception:
        raise HTTPException(status_code=500, detail="Could not delete post.")


@router.get("/competitions", response_model=List[int])
def get_competition_ids():
    competition_ids = post_queries.get_all_competitions()
    return competition_ids
