from fastapi import APIRouter, Response, Depends
from typing import Union, List
from queries.blog import BlogQueries, BlogIn, BlogOut, Error

router = APIRouter()


@router.get("/blogs", response_model=List[BlogOut])
def get_all_blogs(
    response: Response,
    blog: BlogQueries = Depends(),
):
    try:
        return blog.get_all()
    except Exception:
        response.status_code = 400
        return {"message": "Could not retrieve blogs."}


@router.post("/blogs", response_model=Union[BlogOut, Error])
def create_blog(
    info: BlogIn,
    response: Response,
    blog: BlogQueries = Depends(),
):
    try:
        if not info.title or not info.body:
            response.status_code = 400
            return {"message": "Title and body are required."}
        return blog.create(info)
    except Exception:
        response.status_code = 400
        return {"message": "Could not create blog."}


@router.put("/blogs/{blog_id}", response_model=Union[BlogOut, Error])
def update_blog(
    blog_id: int,
    response: Response,
    blog: BlogIn,
    repo: BlogQueries = Depends(),
):
    try:
        return repo.update(blog_id, blog)
    except Exception:
        response.status_code = 400
        return {"message": "Could not update a blog :("}


@router.delete("/blogs/{blog_id}", response_model=Union[bool, Error])
def delete_blog(
    blog_id: int,
    response: Response,
    repo: BlogQueries = Depends(),
) -> bool:
    try:
        return repo.delete(blog_id)
    except Exception:
        response.status_code = 404
        return {"message": "Could not delete a blog by that ID."}
