from fastapi import APIRouter, Depends, Response
from typing import List, Union
from queries.plant import Error, PlantIn, PlantRepository, PlantOut

router = APIRouter()


@router.post("/api/plant", response_model=Union[PlantOut, Error])
def create_plant(
    plant: PlantIn,
    response: Response,
    repo: PlantRepository = Depends(),
):
    try:
        if not plant.name or not plant.image or not plant.price:
            response.status_code = 400
            return Error(message="Name, image, and price are required.")
        created_plant = repo.create(plant)
        return created_plant
    except Exception:
        response.status_code = 500
        return Error(message="Could not create plant.")


@router.get("/api/plant/{id}", response_model=Union[Error, List[PlantOut]])
def get_plant(
    id: int,
    repo: PlantRepository = Depends(),
):
    try:
        return repo.get_plant(id)
    except Exception:
        return Error(message="plant not found")


@router.get("/api/plant", response_model=Union[Error, List[PlantOut]])
def get_all_plant(
    repo: PlantRepository = Depends(),
):
    return repo.get_all_plants()


@router.put("/api/plant/{plant_id}", response_model=Union[PlantOut, Error])
def update_plant(
    plant_id: int,
    plant: PlantIn,
    response: Response,
    repo: PlantRepository = Depends(),
):
    try:
        updated_plant = repo.update(plant_id, plant)
        return updated_plant
    except Exception:
        response.status_code = 404
        return Error(message="Plant not found.")


@router.delete("/api/plant/{plant_id}", response_model=bool)
def delete_plant(
    plant_id: int,
    response: Response,
    repo: PlantRepository = Depends(),
):
    try:
        success = repo.delete(plant_id)
        return success
    except Exception:
        response.status_code = 404
        return False
