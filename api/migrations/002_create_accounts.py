steps = [
    [
        """
        CREATE TABLE accounts (
            id SERIAL PRIMARY KEY NOT NULL,
            username VARCHAR(200) NOT NULL UNIQUE,
            email VARCHAR(200) NOT NULL UNIQUE,
            password VARCHAR(200) NOT NULL,
            first VARCHAR(200) NOT NULL,
            last VARCHAR(200) NOT NULL
        );
        """,
        """
        DROP TABLE accounts;
        """,
    ]
]
