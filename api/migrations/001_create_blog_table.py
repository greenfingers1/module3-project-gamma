steps = [
    [
        """
        Create table blog (
            id serial primary key not null,
            title varchar(150) not null,
            body text not null,
            image varchar(400),
            author_id INT NOT NULL,
            created_on timestamp not null default current_timestamp
        );
        """,
        """
        DROP table blog;
        """,
    ]
]
