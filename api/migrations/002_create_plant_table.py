steps = [
    [
        """
        Create table catalog (
            id serial primary key not null,
            name varchar(150) not null,
            price money not null,
            description text not null,
            image varchar(400)
        );
        """,
        """
        DROP table catalog;
        """,
    ]
]
