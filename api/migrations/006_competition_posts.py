steps = [
    [
        """
        CREATE TABLE post (
            id SERIAL PRIMARY KEY NOT NULL,
            user_name VARCHAR(100) NOT NULL,
            picture TEXT,
            description TEXT NOT NULL,
            created_on TIMESTAMP NOT NULL DEFAULT NOW(),
            competition_id INT NOT NULL
        );
        """,
        """
        DROP TABLE post;
        """,
    ]
]
