steps = [
    [
        ## Create the table
        """
        CREATE TABLE plants (
        id SERIAL PRIMARY KEY NOT NULL,
        name VARCHAR(1000) NOT NULL,
        price VARCHAR NOT NULL,
        image VARCHAR -- Assuming the image field will store in the URL as a string
        )
        """,
        ## Drop the table
        """
        DROP TABLE plants;
        """,
    ]
]
