steps = [
    [
        ## Create the table
        """
        CREATE TABLE catalog_gallery (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(100) NOT NULL,
            price DOUBLE PRECISION NOT NULL,
            image VARCHAR(400) NOT NULL,
            description TEXT NOT NULL
        );
        """,
        ## Drop the table
        """
        DROP TABLE catalog_gallery;
        """,
    ]
]
