steps = [
    [
        """
        ALTER TABLE accounts
        ADD COLUMN role VARCHAR(200) NOT NULL DEFAULT 'member'
        """,
        """
        DROP TABLE accounts
        """,
    ]
]
