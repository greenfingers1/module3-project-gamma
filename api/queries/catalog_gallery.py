from pydantic import BaseModel
from typing import Optional, List, Union
from queries.pool import pool


class Error(BaseModel):
    message: str


class CatalogIn(BaseModel):
    name: str
    price: float
    image: str
    description: Optional[str]


class CatalogOut(BaseModel):
    id: int
    name: str
    price: float
    image: str
    description: Optional[str]


class CatalogRepository:
    def get_all(self) -> Union[Error, List[CatalogOut]]:
        try:
            # connect db
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    # Run SELECT statement
                    result = db.execute(
                        """
                        SELECT id, name, price, image, description
                        FROM catalog_gallery
                        ORDER BY id;
                        """
                    )
                    result = []
                    for record in db:
                        catalog = CatalogOut(
                            id=record[0],
                            name=record[1],
                            price=record[2],
                            image=record[3],
                            description=record[4],
                        )
                        result.append(catalog)
                    return result
        except Exception as e:
            print(e)
            return {"message": "Could not get all plant catalogs"}

    def create(self, catalog: CatalogIn) -> CatalogOut:
        # connect db
        with pool.connection() as conn:
            # get a cursor(run SQL with)
            with conn.cursor() as db:
                # Run INSERT statement
                result = db.execute(
                    """
                    INSERT INTO catalog_gallery
                        (name, price, image, description)
                    VALUES
                        (%s, %s, %s, %s)
                    RETURNING id;
                    """,
                    [
                        catalog.name,
                        catalog.price,
                        catalog.image,
                        catalog.description,
                    ],
                )
                id = result.fetchone()[0]
                # Return new data.
                old_data = catalog.dict()
                # return {"message": "error!"}
                return CatalogOut(id=id, **old_data)
