from pydantic import BaseModel, ValidationError
from typing import Optional, List, Union
from .pool import pool


class Error(BaseModel):
    message: str


class PlantIn(BaseModel):
    name: str
    image: Optional[str]
    price: float


class PlantOut(BaseModel):
    id: int
    name: str
    image: Optional[str]
    price: float


class PlantRepository:
    def update(self, plant_id: int, plant: PlantIn) -> Union[PlantOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE plants
                        SET name = %s
                        , image = %s
                          , price = %s
                        WHERE id = %s
                        """,
                        [plant.name, plant.image, plant.price, plant_id],
                    )
                    return self.plant_in_to_out(plant_id, plant)
        except ValidationError as e:
            print(e)
            return Error(message="Validation error")
        except Exception as e:
            print(e)
            return Error(message="Could not update plant")

    def delete(self, plant_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM plants
                        WHERE id = %s
                        """,
                        [plant_id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def get_all_plants(self) -> Union[Error, List[PlantOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, name, image, price
                        FROM plants
                        ORDER BY name;
                        """
                    )
                    result = []
                    for record in db:
                        plant = PlantOut(
                            id=record[0],
                            name=record[1],
                            image=record[2],
                            price=record[3],
                        )
                        result.append(plant)
                    return result
        except Exception as e:
            print(e)
            return Error(message="Could not get plants")

    def get_plant(self, id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        f"SELECT id, name, image, price "
                        f"FROM plants "
                        f"WHERE id = {id}"
                        f" ORDER BY name;"
                    )
                    result = []
                    for record in db:
                        plant = PlantOut(
                            id=record[0],
                            name=record[1],
                            image=record[2],
                            price=record[3],
                        )
                        result.append(plant)
                    return result
        except Exception as e:
            print(e)
            return Error(message="Could not get plants")

    def create(self, plant: PlantIn) -> PlantOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO plants
                            (name, image, price)
                        VALUES
                            (%s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            plant.name,
                            plant.image,
                            plant.price,
                        ],
                    )
                    id = result.fetchone()[0]
                    return self.plant_in_to_out(id, plant)
        except ValidationError as e:
            print(e)
            return Error(message="Validation error")
        except Exception as e:
            print(e)
            return Error(message="Create did not work")

    def plant_in_to_out(self, id: int, plant: PlantIn):
        old_data = plant.dict()
        return PlantOut(id=id, **old_data)
