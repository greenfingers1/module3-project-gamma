from pydantic import BaseModel
from typing import List, Union
from datetime import datetime
from .pool import pool


class Error(BaseModel):
    message: str


class PostOut(BaseModel):
    id: int
    user_name: str
    picture: str
    description: str
    created_on: datetime = datetime.now()


class PostIn(BaseModel):
    user_name: str
    picture: str
    description: str


class PostQueries:
    def get_all(self, competition_id: int) -> Union[List[PostOut], Error]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                result = cur.execute(
                    """
                    SELECT id, user_name, picture, description, created_on
                    FROM post
                    WHERE competition_id = %s
                    ORDER BY created_on DESC
                    """,
                    [competition_id],
                )
                return [self.record_to_post_out(record) for record in result]

    def record_to_post_out(self, record):
        return PostOut(
            id=record[0],
            user_name=record[1],
            picture=record[2],
            description=record[3],
            created_on=record[4],
        )

    def create(
        self, competition_id: int, post: PostIn
    ) -> Union[PostOut, Error]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                result = cur.execute(
                    """
                    INSERT INTO post(user_name,picture,description,
                    competition_id)
                    VALUES (%s, %s, %s, %s)
                    RETURNING id, created_on;
                    """,
                    [
                        post.user_name,
                        post.picture,
                        post.description,
                        competition_id,
                    ],
                )
                id = result.fetchone()[0]
                return self.post_in_to_out(id, post)

    def post_in_to_out(self, id: int, post: PostIn) -> PostOut:
        old_data = post.dict()
        return PostOut(
            id=id,
            **old_data,
        )

    def update(
        self, competition_id: int, post_id: int, post: PostIn
    ) -> Union[PostOut, Error]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    UPDATE post
                    SET user_name = %s, picture = %s, description = %s
                    WHERE id = %s AND competition_id = %s;
                    """,
                    [
                        post.user_name,
                        post.picture,
                        post.description,
                        post_id,
                        competition_id,
                    ],
                )
                return self.post_in_to_out(post_id, post)

    def delete(self, competition_id: int, post_id: int) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    DELETE FROM post
                    WHERE id = %s AND competition_id = %s;
                    """,
                    [post_id, competition_id],
                )
                return True

    def get_all_competitions(self) -> Union[List[int], Error]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT DISTINCT competition_id
                    FROM post;
                    """
                )
                result = cur.fetchall()
                return [record[0] for record in result]
