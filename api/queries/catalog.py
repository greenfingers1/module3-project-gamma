import os
import json
from pydantic import BaseModel
from typing import List


class Plant(BaseModel):
    name: str
    price: float
    description: str
    id: int


class PlantQueries(BaseModel):
    plants: List[Plant]


def get_plants(directory):
    plant_list = []
    for filename in os.listdir(directory):
        file_path = os.path.join(directory, filename)
        if os.path.isfile(file_path) and filename.endswith(".json"):
            with open(file_path, "r") as file:
                try:
                    plant_data = json.load(file)
                    plant_list.append(plant_data)
                except json.JSONDecodeError:
                    print(f"Error parsing JSON file: {file_path}")
    return plant_list


def print_plant_info(plants):
    for plant in plants:
        name = plant.name
        price = plant.price
        description = plant.description
        plant_id = plant.id
        print(f"Name: {name}")
        print(f"Price: {price}")
        print(f"Description: {description}")
        print(f"ID: {plant_id}")
        print()


# Directory path where the plant data files are located
# query_directory = '/path/to/your/query/directory'

# plants = get_plants(query_directory)
# try:
#     plant_queries = PlantQueries(plants=plants)
#     print_plant_info(plant_queries.plants)
# except Exception as e:
#     print(f"Error processing plant data: {str(e)}")
