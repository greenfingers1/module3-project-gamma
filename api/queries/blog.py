from datetime import datetime
import requests
from typing import Optional, List, Union

from pydantic import BaseModel
from .keys import PEXELS_API_KEY
from .pool import pool


class Error(BaseModel):
    message: str


class BlogOut(BaseModel):
    id: int
    title: str
    body: str
    image: Optional[str]
    created_on: datetime = datetime.now()
    author_id: int


class BlogIn(BaseModel):
    title: str
    body: str
    image: Optional[str]
    author_id: int


class BlogQueries:
    def get_all(self) -> Union[List[BlogOut], Error]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                result = cur.execute(
                    """
                    SELECT id, title, body, image, created_on, author_id
                    FROM blog
                    ORDER BY created_on DESC
                    """
                )
                return [self.record_to_blog_out(record) for record in result]

    def record_to_blog_out(self, record):
        return BlogOut(
            id=record[0],
            title=record[1],
            body=record[2],
            image=record[3],
            created_on=record[4],
            author_id=record[5],
        )

    def create(
        self,
        blog: BlogIn,
    ) -> Union[BlogOut, Error]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                if not blog.image:
                    try:
                        response = requests.get(
                            "https://api.pexels.com/v1/search",
                            headers={"Authorization": f"{PEXELS_API_KEY}"},
                            params={"query": blog.title, "per_page": 1},
                        )
                        response.raise_for_status()
                        data = response.json()
                        if data["photos"]:
                            blog.image = data["photos"][0]["src"]["large"]
                    except requests.RequestException as e:
                        return Error(message=str(e))

                result = cur.execute(
                    """
                    INSERT INTO blog (title, body, image, author_id)
                    VALUES (%s, %s, %s, %s)
                    RETURNING id, created_on;
                    """,
                    [
                        blog.title,
                        blog.body,
                        blog.image,
                        blog.author_id,
                    ],
                )
                id = result.fetchone()[0]
                return self.blog_in_to_out(
                    id,
                    blog,
                )

    def blog_in_to_out(
        self,
        id: int,
        blog: BlogIn,
    ) -> BlogOut:
        old_data = blog.dict()
        return BlogOut(
            id=id,
            **old_data,
        )

    def update(
        self,
        blog_id: int,
        blog: BlogIn,
    ) -> Union[BlogOut, Error]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    UPDATE blog
                    SET title = %s, body = %s, image = %s, author_id = %s
                    WHERE id = %s;
                    """,
                    [
                        blog.title,
                        blog.body,
                        blog.image,
                        blog.author_id,
                        blog_id,
                    ],
                )
                return self.blog_in_to_out(
                    blog_id,
                    blog,
                )

    def delete(self, blog_id: int) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    DELETE FROM blog
                    WHERE id = %s
                    """,
                    [blog_id],
                )
                return True
