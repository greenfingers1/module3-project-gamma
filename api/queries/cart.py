from pydantic import BaseModel
from typing import Optional, List, Union
from .pool import pool


class Error(BaseModel):
    message: str


class CartIn(BaseModel):
    name: str
    image: Optional[str]
    price: float
    quantity: int


class CartOut(BaseModel):
    id: int
    name: str
    image: Optional[str]
    price: float
    quantity: int


class CartRepository:
    def update(self, plant_id: int, cart: CartIn) -> Union[CartOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE cart
                        SET name = %s
                          , image = %s
                          , price = %s
                          , quantity = %s
                        WHERE id = %s
                        """,
                        [
                            cart.name,
                            cart.image,
                            cart.price,
                            cart.quantity,
                            plant_id,
                        ],
                    )
                    return self.cart_in_to_out(plant_id, cart)
        except Exception as e:
            print(e)
            return {"message": "Could not update cart"}

    def delete(self, plant_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM cart
                        WHERE id = %s
                        """,
                        [plant_id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def get_all_cart(self) -> Union[Error, List[CartOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, name, image, price, quantity
                        FROM cart
                        ORDER BY image;
                        """
                    )
                    result = []
                    for record in db:
                        cart = CartOut(
                            id=record[0],
                            name=record[1],
                            image=record[2],
                            price=record[3],
                            quantity=record[4],
                        )
                        result.append(cart)
                    return result
        except Exception as e:
            print(e)
            return {"message": "Could not get cart"}

    def create(self, cart: CartIn) -> CartOut:
        try:
            with pool.connection() as conn:

                with conn.cursor() as db:

                    result = db.execute(
                        """
                        INSERT INTO cart
                            (name, image, price, quantity)
                        VALUES
                            (%s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [cart.name, cart.image, cart.price, cart.quantity],
                    )
                    id = result.fetchone()[0]
                    return self.cart_in_to_out(id, cart)
        except Exception:
            return {"message": "Create did not work"}

    def cart_in_to_out(self, id: int, cart: CartIn):
        old_data = cart.dict()
        return CartOut(id=id, **old_data)
        return CartOut(id=id, **old_data)
