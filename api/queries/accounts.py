from typing import Union

from pydantic import BaseModel

from .pool import pool


class Error(BaseModel):
    message: str


class DuplicateAccountError(ValueError):
    pass


class AccountIn(BaseModel):
    first: str
    last: str
    username: str
    email: str
    password: str


class AccountOut(BaseModel):
    id: int
    first: str
    last: str
    username: str
    email: str
    role: str


class AccountOutWithPassword(AccountOut):
    hashed_password: str


class AccountRepository:
    def get(self, email: str) -> Union[AccountOutWithPassword, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, username, email, first, last, role, password
                        FROM accounts
                        WHERE email = %s OR username = %s;
                        """,
                        [email, email],
                    )
                    account = db.fetchone()
                    return AccountOutWithPassword(
                        id=account[0],
                        username=account[1],
                        email=account[2],
                        first=account[3],
                        last=account[4],
                        role=account[5],
                        hashed_password=account[6],
                    )
        except Exception:
            return

    def create(
        self,
        info: AccountIn,
        hashed_password: str,
    ) -> AccountOutWithPassword:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    INSERT INTO accounts
                        (first, last, username, email, password)
                    VALUES
                        (%s, %s, %s, %s, %s)
                    RETURNING id, role;
                    """,
                    (
                        info.first,
                        info.last,
                        info.username,
                        info.email,
                        hashed_password,
                    ),
                )
                data = db.fetchone()
                id, role = data[0], data[1]
                if id is None:
                    return None
                return AccountOutWithPassword(
                    id=id,
                    username=info.username,
                    email=info.email,
                    hashed_password=hashed_password,
                    first=info.first,
                    last=info.last,
                    role=role,
                )

    def update(
        self, account_id: int, info: AccountIn, hashed_password: str
    ) -> AccountOutWithPassword:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    UPDATE accounts
                    SET first = %s,
                        last = %s,
                        username = %s,
                        email = %s,
                        password = %s
                    WHERE id = %s
                    RETURNING role;
                    """,
                    [
                        info.first,
                        info.last,
                        info.username,
                        info.email,
                        hashed_password,
                        account_id,
                    ],
                )
                role = db.fetchone()[0]
                return AccountOutWithPassword(
                    id=account_id,
                    username=info.username,
                    email=info.email,
                    hashed_password=hashed_password,
                    first=info.first,
                    last=info.last,
                    role=role,
                )

    def make_admin(self, account_id) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    UPDATE accounts
                    SET role = 'admin'
                    WHERE id = %s;
                    """,
                    [account_id],
                )
                return True

    def get_one(self, account_id: int) -> AccountOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id, username, email, first, last, role
                    FROM accounts
                    WHERE id = %s;
                    """,
                    [account_id],
                )
                account = db.fetchone()
                return AccountOut(
                    id=account[0],
                    username=account[1],
                    email=account[2],
                    first=account[3],
                    last=account[4],
                    role=account[5],
                )
