 # Green Fingers - Plant Shop and Blogs

 * Green Fingers is a website designed specifically for new and experiened plant and gardening enthusiasts.
* The platform enables users to create accounts, connect with other gardeners, ask questions, share knowledge, and write blog posts about plants and gardening.
* The platform also features a plant shop which allows users to purchase plants directly from the site.



#### Developers:

* Dylan Vuong - *Plant Shop and Shopping Cart*
* Jacky Li - *Competition and Competition Posts*
* James Lee - *Supplies*
* Michael Nguyen- *Blogs, Login, Sign-in, Auth*


#### [Deployed Platform Here](https://greenfingers1.gitlab.io/module3-project-gamma/)

#### Functionality:

* User Registration and Account Creation:
  - Users can create accounts on the platform by providing their email and setting up a password.
  - After successful registration, users will have personalized accounts to access various features.

* Blogging System:
  - Registered users can write and publish blogs related to their plants and gardening experiences.
  - Blogs can include text, images, and videos to showcase their plants, gardening tips, and experiences.
  - Other users can read, like, and comment on the blogs to engage with each other.

* Community Forums:
  - The platform will have community forums where users can ask questions and seek advice from other gardening enthusiasts.
  - Users can create threads and participate in discussions, exchanging knowledge and insights.

* Collaboration Features:
  - Users can collaborate with each other on specific gardening projects or challenges.
  - They can form groups or join existing ones to work together, share progress, and exchange ideas.

* Plant Shop:
  - The website will feature a plant shop where users can purchase plants, gardening tools, and supplies.
  - Products in the shop will be categorized for easy browsing and searching.

* Shopping Cart and Checkout:
  - Users can add items to their shopping cart while browsing the plant shop.
  - The shopping cart will keep track of selected items and their quantities.
  - Users can proceed to checkout, provide shipping information, and make payments to complete the purchase.

* User Profiles:
  - Each user will have a public profile that showcases their gardening interests and contributions.
  - The profile will display their blog posts, forum activities, collaborations, and more.

* Competition:
  - Users will be able to view a gallery of previous and current competitions that were held on the site.

* Competition Posts:
  - Users will be able to submit a post with a link to their garden image.
    - Users can decide to submit the post under their user name or anonymously.
    - We will implement posts that are directly associated to a users account in the near future, this will also allow them to edit or delete certain posts they have previously made.
    - It is still under discussion if this endpoint will be under auth.
  - A collection of all the participants submission will be displayed in a gallery similar to the competition page.

By incorporating these functionalities, Green Fingers offers a comprehensive and engaging experience for gardening enthusiasts. Users will have a place to showcase their plants, share knowledge, collaborate, and shop for all

#### Project Initalization

To fully enjoy this application on your local machine, please make sure to follow these steps:

1. Download and install **Docker Desktop** [here](https://www.docker.com/products/docker-desktop/)
2. Open Terminal and choose the directory you will be working on by running this command:
	> `cd directory_name` <br><small>(change "directory_name" to your respective directory)</small>
3. Fork and Clone the project from GitLab [here](https://gitlab.com/greenfingers1/module3-project-gamma)
	> `git clone paste_copied_HTTPS_URL_here`
4. Change your working directory to the directory you just cloned
	> `cd project_name`
5. Build the images on Docker
	> `docker-compose build`
6. Run the container from the images you just created
	> `docker-compose up`
7. Open a browser of choice.  Google Chrome is *recommended*.
8. Head over to http://localhost:3000/ in your browser to start browsing through the website!
	- This URL will take you to the homepage of our project, Green Fingers!


	- This is what you should see:

		![Homepage](https://scontent-sjc3-1.xx.fbcdn.net/v/t39.30808-6/363804408_6806569962687362_8616535126774050564_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=730e14&_nc_ohc=Wk-LLdV8UeUAX-BG38M&_nc_ht=scontent-sjc3-1.xx&oh=00_AfDNLN-UcrPe1Z_FT79Tgqcng9HjUaU0z31bYVrMCj_lww&oe=64C83118)

## Wireframe
<details>
  <summary> Wireframing outline </summary>

  ![Homepage](https://scontent-sjc3-1.xx.fbcdn.net/v/t39.30808-6/363790347_6806699856007706_4098232976243122470_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=730e14&_nc_ohc=hfT0s4fFPn8AX-y7zEG&_nc_ht=scontent-sjc3-1.xx&oh=00_AfCCXhW-LY2evgqiujYA9c-jR0mUNNezoIIBwKaHHWYWLw&oe=64C8D766)
</details>

<details>
  <summary>More details ...</summary>

  [Click here](https://excalidraw.com/#room=a25c75bea8bdfca7635b,y-7GATe6swGymaTIzumi2g) to take a closer look our wireframing outline on Excalidraw.
</details>


## APIs
<details>
  <summary> Plant Shop APIs </summary>

  |Action                        |Method|URL                                         |
|------------------------------|------|--------------------------------------------|
|Get all plants            |GET   |http://localhost:8000/api/plant/    |
|Create a plant         |POST  |http://localhost:8000/api/plant/    |
|Get a specific plant   |GET   |http://localhost:8000/api/plant/{plant_id}/|
|Update a specific plant|PUT   |http://localhost:8000/api/plant/{plant_id}/|
|Delete a specific plant |DELETE|http://localhost:8000/api/plant/{plant_id}/|
</details>

<details>
  <summary> Shopping Cart APIs </summary>

|Action                        |Method|URL                                         |
|------------------------------|------|--------------------------------------------|
|Get all items in Cart            |GET   |http://localhost:8000/api/cart/    |
|Add an item to Cart         |POST  |http://localhost:8000/api/cart/    |
|Update Cart  |PUT   |http://localhost:8000/api/cart/{plant_id}/|
|Delete a specific item in Cart |DELETE|http://localhost:8000/api/cart/{plant_id}/|
</details>

<details>
  <summary> Blog APIs </summary>


|Action                        |Method|URL                                         |
|------------------------------|------|--------------------------------------------|
| Get all blogs | GET | http://localhost:8000/blogs
| Create a blog | POST | http://localhost:8000/blogs
| Update a blog | PUT | http://localhost:8000/blogs/{blog_id}
| Delete a blog | DELETE | http://localhost:8000/blogs/{blog_id}
</details>

<details>
  <summary> Competitions api </summary>


|Action                        |Method|URL                                         |
|------------------------------|------|--------------------------------------------|
| Get all competitions | GET | http://localhost:8000/competitions
| Get all posts tied to a certain competition | GET | http://localhost:8000/competition/{id}/posts
| Create a post | POST | http://localhost:8000/competition/{id}/posts
| Update a post | PUT | http://localhost:8000/competition/{id}/posts/{post_id}
| Delete a post | DELETE | http://localhost:8000/competition/{id}/posts/{post_id}
</details>

<details>
  <summary> Supplies APIs </summary>

|Action                        |Method|URL                                         |
|------------------------------|------|--------------------------------------------|
|Get all supplies            |GET   |http://localhost:8000/catalog/supplies/    |
|Add an item to supplies         |POST  |http://localhost:8000/catalog/supplies/create/    |
</details>


 ## Design

![Alt text](Blog.png)
![Alt text](Home.png)
![Alt text](login.png)



## Code Demonstration

<details>
  <summary>Plant Shop</summary>

### List All Plants (GET)
http://localhost:8000/api/plant
Returns a list of all plants in the plant shop
``` json
//
[
  {
    "id": 3,
    "name": "Arugula",
    "image": "Arugula.png",
    "price": 2.79
  },
  {
    "id": 2,
    "name": "Mint",
    "image": "Mint.png",
    "price": 0.79
  },
]
```

### Create a Plant (POST)
http://localhost:8000/api/plant
Post a new plant in the plant shop
``` json
//
[
  {
    "id": 3,
    "name": "Arugula",
    "image": "Arugula.png",
    "price": 2.79
  },
]
```

### Get a Specific Plant (GET)
http://localhost:8000/api/plant{id}
Get a specific plant in the shop
Enter a plant id that you want to get
Example id = 3
``` json
//
[
  {
    "id": 3,
    "name": "Arugula",
    "image": "Arugula.png",
    "price": 2.79
  },
]
```

### Update a Specific Plant (PUT)
http://localhost:8000/api/plant{plant_id}
Update a specific plant in the shop
Enter a plant id that you want to update. 
Example: id = 3
``` json
//
[
  {
    "name": "Arugula",
    "image": "Arugula.png",
    "price": 2.79
  },
]
```
### Delete a Specific Plant (DELETE)
http://localhost:8000/api/plant{plant_id}
Delete a specific plant in the shop
Enter a plant id that you want to delete. 
Example: id = 3
system response
``` json
//
true
```
</details>

<details>
  <summary>Cart</summary>

### List All Items in Cart (GET)
http://localhost:8000/api/cart
Returns a list of all items in the shopping cart
``` json
//
[
  { "id": 4,
    "name": "Rose",
    "image": "Rose.png",
    "price": 2.89,
    "quantity": 1
  },
  {
    "id": 3,
    "name": "Mint",
    "image": "Mint.png",
    "price": 0.79,
    "quantity": 1
  }
]
```


### Add an Item to Cart (POST)
http://localhost:8000/api/cart
Add an item to a Cart
``` json
//
[
  {
    "name": "Arugula",
    "image": "Arugula.png",
    "price": 2.79,
    "quantity": 0
  },
]
```

### Delete Items in Cart (POST)
http://localhost:8000/api/cart/{plant_id}
Delete a specific item in Cart
Enter the id of the item you want to delete from Cart
Example id = 4
``` json
//
true
```
</details>

<details>
  <summary>Blog</summary>

### List Blog (GET):
http://localhost:8000/blogs
Returns a list of all blogs
``` json
//
[
  {
    "id": 0,
    "title": "string",
    "body": "string",
    "image": "string",
    "created_on": "2023-07-27T19:12:26.532068",
    "author_id": 0
  }
]
```

### Create a new blog (POST):
http://localhost:8000/blogs

```json
{
  "title": "string",
  "body": "string",
  "image": "string",
  "author_id": 0
}
```

### Update a specific blog (PUT):
http://localhost:8000/blogs/{blog_id}
Updating a blog, change any fields
```json
{
  "title": "string",
  "body": "string",
  "image": "string",
  "author_id": 0
}
```

### Delete a specific blog (DELETE):
http://localhost:8000/blogs/{blog_id}
Delete an blog. Replace {blog_id} in the request url with the blog "id".

</details>

<details>
  <summary>Competitions</summary>

### List Competitions (GET):
http://localhost:8000/competitions
Returns a list of all competitions by id

Example Response:
``` json
[
  1
  2
  3
]
```
### List Posts per Competition (GET):
http://localhost:8000/competition/{id}/posts
Returns a list of all posts pertaining to a certain competition

Example Response:
``` json
[
  {
    "id": 0,
    "user_name": "string",
    "picture": "string",
    "description": "string",
    "created_on": "2023-07-27T19:12:27.058170"
  }
]
```

### Create a new competition post (POST):
http://localhost:8000/competition/{id}/posts
The competition id that the post is tied to will need to be entered

Required JSON body
``` json
{
  "user_name": "string",
  "picture": "string",
  "description": "string"
}
```

Example Response:
``` json
{
  "id": 0,
  "user_name": "string",
  "picture": "string",
  "description": "string",
  "created_on": "2023-07-27T19:12:27.058170"
}
```

### Update a specific post (PUT):
http://localhost:8000/competition/{id}/posts/{post_id}
Updating a post, change any fields

Required JSON body
``` json
{
  "user_name": "Test",
  "picture": "Test.picture_url.com",
  "description": "string"
}
```

Example Response:
``` json
{
  "id": 0,
  "user_name": "Test",
  "picture": "Test.picture_url.com",
  "description": "string",
  "created_on": "2023-07-27T19:12:27.058170"
}
```

### Delete a specific post (DELETE):
http://localhost:8000/competition/{id}/posts/{post_id}
Delete an post. Replace {id} in the request url with the competition "id" and {post_id} with post "id".

Example Response:
``` json
{
  "message": "Post deleted successfully"
}
```
</details>

<details>
  <summary>Supplies</summary>

### List Supplies (GET):
http://localhost:8000/catalog/supplies
Returns a list of all supplies

Example Response:
``` json
[
  {
  "name": "string",
  "price": 0,
  "image": "string",
  "description": "string"
}
]
```


### Create a new supply (POST):
http://localhost:8000/catalog/supplies/create

Example Request:
```json
[
  {
  "name": "string",
  "price": 0,
  "image": "string",
  "description": "string"
}
]
```
<details>
