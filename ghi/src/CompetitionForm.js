import React, { useState, useEffect, useCallback } from 'react';
import axios from 'axios';
import { useParams, useNavigate } from 'react-router-dom';
import styles from './CompetitionForm.module.css';
import DropDownMenu from './DropDownMenu'

const PostHandler = () => {
    const { id: competitionId } = useParams();
    const [posts, setPosts] = useState([]);
    const [newPost, setNewPost] = useState({
        user_name: '',
        picture: '',
        description: '',
    });

    const fetchPosts = useCallback(async () => {
        try {
            const response = await axios.get(`https://mar-11-pt-greenfingers1.mod3projects.com/competition/${competitionId}/posts`);
            setPosts(response.data);
        } catch (error) {
            console.error('Error fetching posts:', error);
        }
    }, [competitionId]);

    useEffect(() => {
        fetchPosts();
    }, [fetchPosts]);

    const [redirectToPosts, setRedirectToPosts] = useState(false);
    const navigate = useNavigate();

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setNewPost((prevPost) => ({ ...prevPost, [name]: value }));
    };

    const createPost = async () => {
        try {
            const response = await axios.post(`https://mar-11-pt-greenfingers1.mod3projects.com/competition/${competitionId}/posts`, newPost);
            setPosts([...posts, response.data]);
            setNewPost({
                user_name: '', //have it automatically input username based on user info as a stretch goal
                picture: '',
                description: '',
            });
            setRedirectToPosts(true);
        } catch (error) {
            console.error('Error creating post:', error);
        }
    };

    useEffect(() => {
        if (redirectToPosts) {
            navigate(`/competitions/${competitionId}/posts`);
        }
    }, [redirectToPosts, navigate, competitionId]);

    return (
        <div className={styles.container}>
            <DropDownMenu />
            <h2 className={styles.heading}>Contribute A Post !</h2>
            <div className={styles.form}>
                <input
                    type="text"
                    name="user_name"
                    placeholder="User Name"
                    value={newPost.user_name}
                    onChange={handleInputChange}
                    className={styles.inputField}
                />
                <input
                    type="text"
                    name="picture"
                    placeholder="Picture URL"
                    value={newPost.picture}
                    onChange={handleInputChange}
                    className={styles.inputField}
                />
                <textarea
                    type="text"
                    name="description"
                    placeholder="Description"
                    value={newPost.description}
                    onChange={handleInputChange}
                    className={styles.inputField}
                />
                <button onClick={createPost} className={styles.createBtn}>Create</button>
            </div>
        </div>
    );
};

export default PostHandler;