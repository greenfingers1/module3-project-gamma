import React, { useState, useEffect } from 'react';
import './Construct.css';
import './Catalog.css';
import { Link } from 'react-router-dom';

function Plant() {
  const [, setPlants] = useState([]);
  const [vegetables, setVegetables] = useState([]);
  const [leafyGreens, setLeafyGreens] = useState([]);
  const [flowers, setFlowers] = useState([]);
  const [housePlants, setHousePlants] = useState([]);
  useEffect(() => {
    fetchPlants();
  }, []);

  const fetchPlants = async () => {
    try {
      const response = await fetch("https://mar-11-pt-greenfingers1.mod3projects.com/api/plant");
      const data = await response.json();
      setPlants(data || []);
      const vegetables = data.filter(plant => plant.price.toFixed(2).endsWith('.69'));
      const leafyGreens = data.filter(plant => plant.price.toFixed(2).endsWith('.79'));
      const flowers = data.filter(plant => plant.price.toFixed(2).endsWith('.89'));
      const housePlants = data.filter(plant => plant.price.toFixed(2).endsWith('.99'));
      setVegetables(vegetables);
      setLeafyGreens(leafyGreens);
      setFlowers(flowers);
      setHousePlants(housePlants);
    } catch (error) {
      console.error("Error fetching plants:", error);
    }
  };

  const fetchPlant = async (id) => {
    try {
      const response = await fetch(`https://mar-11-pt-greenfingers1.mod3projects.com/api/plant/${id}`, {
        method: "GET",
      });
      const data = await response.json();

      return data;
    } catch (error) {
      console.error("Error fetching a single plant.", error);
      return 0;
    }
  };

  const handleAddToCart = (plantId) => {

    fetchPlant(plantId)
      .then((r) => {
        const thePlant = r[0];
        console.log(thePlant);
        const response = fetch('https://mar-11-pt-greenfingers1.mod3projects.com/api/cart',
          {
            method: 'POST',

            body: JSON.stringify({
              "name": thePlant.name,
              "price": thePlant.price,
              "image": thePlant.image,
              "quantity": 1
            }),
            headers: {
              'Content-type': 'application/json; charset=UTF-8',
            },
          })
        return response;
      })
      .then((res) => { console.log(res); })
      .catch((e) => {
        console.log(e);
      })

  };

  return (
    <div className="App">
      <header className="App-header">
        <div className="top-bar">
          <ul>
            <li><Link to="/home">Home Page</Link></li>
            <li><Link to="/blogs">Blog</Link></li>
            <li><Link to="/catalog">Plant Shop</Link></li>
            <li><Link to="/catalog/supplies">Supplies</Link></li>
            <li><Link to="/cart">Cart</Link></li>
            <li><Link to="/competitions">Competitions</Link></li>
          </ul>
        </div>
      </header>
      <div>
        <h1>Welcome to Green Fingers Greenhouse!</h1>
      </div>
      <div id="body" className="grid-container">
        <div className="categories-wrapper">
          <div className="column">
            <h2>Vegetables</h2>
            <div className="plants-grid">
              {vegetables.map((plant) => (
                <div className="grid-item" key={plant.id}>
                  <div className='plant-name-wrapper'>
                    <span id="plant-name">{plant.name}</span>
                  </div>
                  <img src={plant.image} alt={plant.name} />
                  <p>{plant.description}</p>
                  <p>Price: ${plant.price}</p>
                  <button className="add-to-cart-button" onClick={() => handleAddToCart(plant.id)}>
                    Add to Cart
                  </button>
                </div>
              ))}
            </div>
          </div>
          <div className="column">
            <h2>Leafy Greens</h2>
            <div className="plants-grid">
              {leafyGreens.map((plant) => (
                <div className="grid-item" key={plant.id}>
                  <div className='plant-name-wrapper'>
                    <span id="plant-name">{plant.name}</span>
                  </div>
                  <img src={plant.image} alt={plant.name} />
                  <p>{plant.description}</p>
                  <p>Price: ${plant.price}</p>
                  <button className="add-to-cart-button" onClick={() => handleAddToCart(plant.id)}>
                    Add to Cart
                  </button>
                </div>
              ))}
            </div>
          </div>
          <div className="column">
            <h2>Flowers</h2>
            <div className="plants-grid">
              {flowers.map((plant) => (
                <div className="grid-item" key={plant.id}>
                  <div className='plant-name-wrapper'>
                    <span id="plant-name">{plant.name}</span>
                  </div>
                  <img src={plant.image} alt={plant.name} />

                  <p>Price: ${plant.price}</p>
                  <button className="add-to-cart-button" onClick={() => handleAddToCart(plant.id)}>
                    Add to Cart
                  </button>
                </div>
              ))}
            </div>
          </div>
          <div className="column">
            <h2>Houseplants</h2>
            <div className="plants-grid">
              {housePlants.map((plant) => (
                <div className="grid-item" key={plant.id}>
                  <div className='plant-name-wrapper'>
                    <span id="plant-name">{plant.name}</span>
                  </div>
                  <img className="plant-img" src={plant.image} alt={plant.name} />
                  <p>{plant.description}</p>
                  <p>Price: ${plant.price}</p>
                  <button className="add-to-cart-button" onClick={() => handleAddToCart(plant.id)}>
                    Add to Cart
                  </button>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Plant;
