const startYear = 2023;
const seasons = ['Summer', 'Autumn', 'Winter', 'Spring'];

export const getCompetitionYearSeason = (competitionId) => {
    const yearIncrease = Math.floor((competitionId - 1) / 4);
    const seasonIndex = (competitionId - 1) % 4;
    return `${startYear + yearIncrease} ${seasons[seasonIndex]}`;
};
