import React, { useState, useEffect, useCallback } from 'react';
import axios from 'axios';
import { useParams, Link } from 'react-router-dom';
import styles from './CompetitionPosts.module.css';
import { getCompetitionYearSeason } from './utils';
import DropDownMenu from './DropDownMenu'

const CompetitionPosts = () => {
    const [posts, setPosts] = useState([]);
    const { id } = useParams();
    const competitionYearSeason = getCompetitionYearSeason(id);

    const fetchCompetitionPosts = useCallback(async () => {
        try {
            const response = await axios.get(`https://mar-11-pt-greenfingers1.mod3projects.com/competition/${id}/posts`);
            setPosts(response.data);
        } catch (error) {
            console.error('Error fetching competition posts:', error);
        }
    }, [id]);

    useEffect(() => {
        fetchCompetitionPosts();
    }, [fetchCompetitionPosts]);

    return (
        <div className={styles.container}>
            <DropDownMenu />
            <Link to="/competitions" className={styles.backBtn}>
                <button>Back to Competitions</button>
            </Link>
            <h2 className={styles.heading}>Posts for {competitionYearSeason} Competition</h2>
            <Link to={`/competitions/${id}/posts/create`} className={styles.submitBtn}>
                <button>Submit Entry</button>
            </Link>
            {posts.length === 0 ? (
                <p>No posts available for this competition.</p>
            ) : (
                <ul className={styles.postsList}>
                    {posts.map((post) => (
                        <li key={post.id} className={styles.postItem}>
                            <img src={post.picture} alt="Post" />
                            <p className={styles.userName}>Username: {post.user_name}</p>
                            <p className={styles.description}>{post.description}</p>
                            <p className={styles.createdAt}>
                                {new Date(post.created_on).toLocaleDateString("en-US")}
                            </p>
                        </li>
                    ))}
                </ul>
            )}
        </div>
    );
};

export default CompetitionPosts;
