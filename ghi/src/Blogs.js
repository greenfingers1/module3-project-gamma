import React, { useState, useEffect } from 'react';
import { useGetBlogsQuery } from './store/BlogApi';
import BlogCard from './BlogCard';
import MenuToggle from './MenuToggle';
import Menu from './Menu';

const BlogListPage = () => {
  const { data: blogs, isLoading, isError } = useGetBlogsQuery();
  const [isOpen, setIsOpen] = useState(false);
  const [scrollY, setScrollY] = useState(0);

  const toggleMenu = () => setIsOpen(!isOpen);

  useEffect(() => {
    const handleScroll = () => {
      setScrollY(window.scrollY);
    };

    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  if (isLoading) {
    return <h2 className="text-center text-2xl mt-12">Loading...</h2>;
  }

  if (isError) {
    return <h2 className="text-center text-2xl mt-12">Error: Unable to fetch blogs</h2>;
  }

  return (
    <div className="App">
      <MenuToggle toggle={toggleMenu} isOpen={isOpen} scrollY={scrollY} />
      <Menu isOpen={isOpen} toggle={toggleMenu} />
      <header className="App-header"></header>
      <div className="flex flex-wrap justify-around p-4">
        {blogs &&
          blogs.map((blog) => (
            <BlogCard key={blog.id} blog={blog} />
          ))}
      </div>
    </div>
  );
};

export default BlogListPage;
