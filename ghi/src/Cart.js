import React, { useEffect, useState } from "react";
import "./Construct.css";
import "./Catalog.css";
import { Link } from 'react-router-dom';

function Cart() {
  const [cartItems, setCartItems] = useState([]);
  useEffect(() => {
    fetchCartItems();
  }, []);

  const fetchCartItems = async () => {
    try {
      const response = await fetch("https://mar-11-pt-greenfingers1.mod3projects.com/api/cart");
      const data = await response.json();
      setCartItems(data || []);
    } catch (error) {
      console.error("Error fetching cart items:", error);
    }
  };

  const handleDeleteItem = async (plant_id) => {
    try {
      const response = await fetch(`https://mar-11-pt-greenfingers1.mod3projects.com/api/cart/${plant_id}`, {
        method: "DELETE",
      });
      if (response.ok) {

        fetchCartItems();
      } else {
        console.error("Failed to delete item from cart:", response.status);
      }
    } catch (error) {
      console.error("Error deleting item from cart:", error);
    }
  };

  const calculateSubtotal = (price, quantity) => {
    return (price * quantity).toFixed(2);
  };

  const calculateTotal = () => {
    const total = cartItems.reduce(
      (accumulator, item) =>
        accumulator + parseFloat(item.price) * item.quantity,
      0
    );
    return total.toFixed(2);
  };

  return (
    <div className="App">
      <header className="App-header">
        <div className="top-bar">
          <ul>
            <li>
              <Link to="/home">Home Page</Link>
            </li>
            <li>
              <Link to="/">Blog</Link>
            </li>
            <li>
              <Link to="/">Competition</Link>
            </li>
            <li>
              <Link to="/catalog">Plant shop</Link>
            </li>
            <li>
              <Link to="/catalog/supplies">Supplies</Link>
            </li>
            <li>
              <Link to="/cart">Cart</Link>
            </li>
          </ul>
        </div>
      </header>
      <div>
        <h1>Your Shopping Cart!</h1>
      </div>
      <div id="body" className="cart-container">
        {cartItems.length > 0 ? (
          <table className="cart-table">
            <thead>
              <tr>
                <th>Item Name</th>
                <th>Item Price</th>
                <th>Item Image</th>
                <th>Quantity</th>
                <th>Subtotal</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {cartItems.map((item) => (
                <tr key={item.id}>
                  <td>{item.name}</td>
                  <td>{item.price}</td>
                  <td>
                    {item.image && (
                      <img
                        src={item.image}
                        alt={item.name}
                        className="cart-item-image"
                      />
                    )}
                  </td>
                  <td>
                    <p>{item.quantity}</p>
                  </td>
                  <td>{calculateSubtotal(item.price, item.quantity)}</td>
                  <td>
                    <button
                      onClick={() => handleDeleteItem(item.id)}
                      className="delete-button"
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              ))}
              <tr>
                <td colSpan="4" className="total-label">
                  Order Total
                </td>
                <td className="total-amount" colSpan="2">
                  {calculateTotal()}
                </td>
              </tr>
            </tbody>
          </table>
        ) : (
          <p>Your Cart Is Empty! Let's Add Something To It!</p>
        )}
        <div className="payment-details">
          <div className="payment">
            <h2>Payment Details</h2>
            <input type="text" placeholder="Customer Name" />
            <input type="text" placeholder="Customer Phone Number" />
            <input type="text" placeholder="Customer Email" />
            <input type="text" placeholder="Enter payment card number" />
          </div>
          <div className="shipping">
            <h2>Shipping Address</h2>
            <input type="text" placeholder="Street Address" />
            <input type="text" placeholder="Apt #/ P.O Box" />
            <input type="text" placeholder="City" />
            <input type="text" placeholder="State" />
          </div>
          <button className=" add-to-cart-button">Submit Order</button>
        </div>
      </div>
    </div>
  );
}

export default Cart;
