import { configureStore } from "@reduxjs/toolkit";
import { authenticator } from "./authenticator";
import { BlogApi } from "./BlogApi";
import { setupListeners } from "@reduxjs/toolkit/dist/query";
import { userSlice } from "./user";

//
export const store = configureStore({
    reducer: {
        auth: userSlice.reducer,
        [authenticator.reducerPath]: authenticator.reducer,
        [BlogApi.reducerPath]: BlogApi.reducer,
    },
    middleware: getDefaultMiddleware =>
        getDefaultMiddleware()
            .concat(authenticator.middleware)
            .concat(BlogApi.middleware),
});

setupListeners(store.dispatch);
