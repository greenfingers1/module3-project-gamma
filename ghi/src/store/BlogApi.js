import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const BlogApi = createApi({
    reducerPath: 'blogApi',
    baseQuery: fetchBaseQuery({
        baseUrl: process.env.REACT_APP_API_HOST,
        credentials: 'include',
    }),
    endpoints: (builder) => ({
        getBlogs: builder.query({
            query: () => '/blogs',
        }),
    })
})

export const {
    useGetBlogsQuery,
} = BlogApi;
