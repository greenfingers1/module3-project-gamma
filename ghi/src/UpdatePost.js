import React, { useState } from 'react';
import axios from 'axios';
import { useParams, useNavigate } from 'react-router-dom';

const UpdatePostForm = ({ id, existingPostData }) => {
    const { id: competitionId } = useParams();
    const [updatedPost, setUpdatedPost] = useState({
        user_name: existingPostData.user_name || '',
        picture: existingPostData.picture || '',
        description: existingPostData.description || '',
    });

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setUpdatedPost((prevPost) => ({ ...prevPost, [name]: value }));
    };

    const updatePost = async () => {
        try {
            const response = await axios.put(
                `http://localhost:8000/competition/${competitionId}/posts/${id}`,
                updatedPost
            );
            // Handle the response if needed
            console.log('Post updated successfully:', response.data);
        } catch (error) {
            console.error('Error updating post:', error);
        }
    };

    const navigate = useNavigate();

    const cancelUpdate = () => {
        // Implement logic to cancel the update and navigate to the previous page if needed
        navigate(`/competitions/${competitionId}/posts`);
    };

    return (
        <div>
            <h2>{existingPostData ? 'Update Post' : 'Create New Post'}</h2>
            <div>
                <input
                    type="text"
                    name="user_name"
                    placeholder="User Name"
                    value={updatedPost.user_name}
                    onChange={handleInputChange}
                />
                <input
                    type="text"
                    name="picture"
                    placeholder="Picture URL"
                    value={updatedPost.picture}
                    onChange={handleInputChange}
                />
                <input
                    type="text"
                    name="description"
                    placeholder="Description"
                    value={updatedPost.description}
                    onChange={handleInputChange}
                />
                <button onClick={updatePost}>{existingPostData ? 'Update' : 'Create'}</button>
                <button onClick={cancelUpdate}>Cancel</button>
            </div>
        </div>
    );
};

export default UpdatePostForm;
