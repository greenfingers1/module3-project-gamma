import React from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import { Link } from 'react-router-dom';

const variants = {
    open: {
        opacity: 1,
        x: 0,
        transition: { staggerChildren: 0.05, delayChildren: 0.2 },
    },
    closed: {
        opacity: 0,
        x: '-100%',
        transition: { staggerChildren: 0.05, staggerDirection: -1 },
    },
};

const childVariants = {
    open: { y: 0, opacity: 1 },
    closed: { y: 50, opacity: 0 },
};

const Menu = ({ isOpen, toggle }) => (
    <AnimatePresence>
        {isOpen && (
            <motion.div className="menu" variants={variants} initial="closed" animate={isOpen ? 'open' : 'closed'}>
                <motion.ul>
                    {[
                        { name: 'Home Page', link: '/home' },
                        { name: 'Blog', link: '/blogs' },
                        { name: 'Competition', link: '/' },
                        { name: 'Catalog/ Plant shop', link: '/catalog' },
                        { name: 'Supplies', link: '/catalog/supplies' },
                        { name: 'Login', link: '/login' },
                        { name: 'Signup', link: '/signup' },
                    ].map((item) => (
                        <motion.li variants={childVariants} key={item.name}>
                            <Link to={item.link}>{item.name}</Link>
                        </motion.li>
                    ))}
                </motion.ul>
            </motion.div>
        )}
    </AnimatePresence>
);

export default Menu;
