import React from 'react';
import { Routes, Route, BrowserRouter, Navigate } from 'react-router-dom';
import Signup from './Signup.js';
import LoginForm from './LoginForm.js';
import Construct from "./Construct.js";
import { useGetTokenQuery } from "./store/authenticator.js";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Catalog from './Catalog.js';
import Greenhouse from './Greenhouse.js';
import Cart from './Cart.js';
import BlogListPage from './Blogs.js';
import ListSupplies from './Supplies.js';
import CreateSupply from './CreateSupplyForm.js';
import CompetitionsList from './Competition.js';
import CompetitionPosts from './CompetitionSpecifc.js';
import PostHandler from './CompetitionForm.js';
import UpdatePostForm from './UpdatePost.js';

function App() {
  const { data } = useGetTokenQuery();
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, '');

  const Protected = ({ token, children }) => {
    return token === null ? <Navigate to="/login" replace /> : children;
  };


  return (
    <BrowserRouter basename={basename}>
      <Routes>
        <Route path="/home" element={<Construct />} />
        <Route path="/" element={<Construct />} />
        <Route path="/login" element={<LoginForm token={data} />} />
        <Route path="/signup" element={<Signup token={data} />} />
        <Route path="cart" element={<Cart />} />
        <Route path="/blogs" element={<Protected token={data}><BlogListPage /></Protected>} />
        <Route path="/catalog" element={<Catalog />} />
        <Route path="/catalog/greenhouse" element={<Greenhouse />} />
        <Route path="/catalog/supplies" element={<ListSupplies />} />
        <Route path="/catalog/supplies/create" element={<CreateSupply />} />
        <Route path="/competitions" element={<CompetitionsList />} />
        <Route path="/competitions/:id/posts" element={<CompetitionPosts />} />
        <Route path="/competitions/:id/posts/create" element={<PostHandler />} />
        <Route path="/competitions/:id/posts/:postId" element={<UpdatePostForm />} />
      </Routes>
      <ToastContainer position='bottom-right' />
    </BrowserRouter>
  );
}

export default App;
