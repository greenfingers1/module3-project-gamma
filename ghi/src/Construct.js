import React from "react";
import "./Construct.css";
import { Link, useNavigate } from 'react-router-dom';
import { useLogoutUserMutation } from './store/authenticator';

function Construct(props) {
  const navigate = useNavigate();
  const [logout] = useLogoutUserMutation();

  const handleLogout = async () => {
    try {
      await logout();
      navigate('/login');
    } catch (error) {
      console.error(error);
    }
  }

  return (
    <div className="App">
      <header className="App-header">
        <div className="top-bar">
          <ul>
            <li><Link to="/home">Home Page</Link></li>
            <li><Link to="/blogs">Blog</Link></li>
            <li><Link to="/catalog">Plant shop</Link></li>
            <li><Link to="/catalog/supplies">Supplies</Link></li>
            <li><Link to="/competitions">Competition</Link></li>
            <li><Link to="/login">Login</Link></li>
            <li><Link to="/signup">Signup</Link></li>
            <li><button onClick={handleLogout}>Logout</button></li>
          </ul>
        </div>
      </header>
      <div className="body">
      </div>
    </div>
  );
}

export default Construct;
