import React from 'react';
import { Link } from 'react-router-dom';
import styles from './DropDownMenu.module.css';

const DropDownMenu = () => (
    <nav className={styles.dropdownMenu}>
        <button className={styles.dropbtn}>Menu</button>
        <div className={styles.dropdownContent}>
            <Link to="/home">Home Page</Link>
            <Link to="/blogs">Blog</Link>
            <Link to="/catalog">Plant shop</Link>
            <Link to="/catalog/supplies">Supplies</Link>
            <Link to="/competitions">Competition</Link>
            <Link to="/login">Login</Link>
            <Link to="/signup">Signup</Link>
        </div>
    </nav>
);

export default DropDownMenu;
