import React from "react";
import "./Construct.css";
import "./Catalog.css";
import { Link } from 'react-router-dom';

function Catalog(props) {

  return (
    <div className="App">
      <header className="App-header">
        <div className="top-bar">
          <ul>
            <li><Link to="/home">Home Page</Link></li>
            <li><Link to="/">Blog</Link></li>
            <li><Link to="/catalog">Plant Shop</Link></li>
            <li><Link to="/catalog/supplies">Supplies</Link></li>
            <li><Link to="/cart">Cart</Link></li>
          </ul>
        </div>
      </header>
      <div>
        <h1>
          Let's add something to your garden!
        </h1>
      </div>
      <div id="body" className="grid-catalog">
        <div className="grid-item1">
          <Link to="/catalog/greenhouse">
            <h2 id="greenhouse-title">Greenhouse This Way</h2>
            <img id="greenhouse" src="https://i.pinimg.com/originals/72/96/68/729668a18b71cb4c36b2b74cb4e138a6.jpg" alt="greenhouse" />
          </Link>
        </div>
      </div>
    </div>
  );
}

export default Catalog;
