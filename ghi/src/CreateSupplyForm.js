import { useEffect, useState } from 'react';



export default function CreateSupply() {
    //const [supply, setSupply] = useState('')
    const [name, setName] = useState('')
    const [price, setPrice] = useState('')
    const [image, setImage] = useState('')
    const [description, setDescription] = useState('')

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    };

    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    };

    const handleImageChange = (event) => {
        const value = event.target.value;
        setImage(value);
    };

    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    };

    useEffect(() => {
        async function ListSupplies() {
            const url = "https://mar-11-pt-greenfingers1.mod3projects.com/catalog/supplies";
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                return data
                // setSupply(data)
            }
        }
        ListSupplies();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.price = price;
        data.image = image;
        data.description = description;

        const url = "https://mar-11-pt-greenfingers1.mod3projects.com/catalog/supplies"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setName("");
            setPrice("");
            setImage("");
            setDescription("");
        }

        window.location.href = 'module3-project-gamma/catalog/supplies';
    };
    return (
        <div>
            <h2>React Form with Four Fields</h2>
            <form onSubmit={handleSubmit}>
                <div>
                    <label htmlFor="name">Name:</label>
                    <input
                        type="text"
                        id="name"
                        name="name"
                        value={name}
                        onChange={handleNameChange}
                    />
                </div>
                <div>
                    <label htmlFor="price">Price:</label>
                    <input
                        type="text"
                        id="price"
                        name="price"
                        value={price}
                        onChange={handlePriceChange}
                    />
                </div>
                <div>
                    <label htmlFor="Image">Image:</label>
                    <input
                        type="text"
                        id="image"
                        name="image"
                        value={image}
                        onChange={handleImageChange}
                    />
                </div>
                <div>
                    <label htmlFor="Description">Description:</label>
                    <textarea
                        id="description"
                        name="description"
                        value={description}
                        onChange={handleDescriptionChange}
                    />
                </div>
                <button type="submit">Submit</button>
            </form>
        </div>
    )

}
