import React, { useState, useEffect } from "react";
import { motion } from "framer-motion";
import './MenuToggle.css';
const Path = (props) => (
    <motion.path
        fill="transparent"
        strokeWidth="3"
        stroke="hsl(0, 0%, 18%)"
        strokeLinecap="round"
        {...props}
    />
);

const variants = {
    open: { d: "M 3 16.5 L 17 2.5" },
    closed: { d: "M 2 2.5 L 20 2.5" },
};

const MenuToggle = ({ toggle, isOpen }) => {
    const [isFixed, setIsFixed] = useState(false);

    useEffect(() => {
        const handleScroll = () => {
            // Get the scroll position
            const scrollY = window.scrollY;
            // Set a threshold value to decide when to fix the button
            const threshold = 100;
            // Determine whether to fix the button or not based on the scroll position
            setIsFixed(scrollY > threshold);
        };

        // Add the event listener for scroll
        window.addEventListener("scroll", handleScroll);

        // Remove the event listener when the component unmounts
        return () => {
            window.removeEventListener("scroll", handleScroll);
        };
    }, []);

    return (
        <div className={`menu-toggle-container${isFixed ? " fixed" : ""}`}>
            <button className="menu-toggle" onClick={toggle}>
                <svg width="100" height="30" viewBox="0 0 23 23">
                    <Path
                        d="M 2 2.5 L 20 2.5"
                        className="top"
                        animate={isOpen ? variants.open : variants.closed}
                        transition={{ duration: 0.1 }}
                    />
                    <Path
                        d="M 2 9.423 L 20 9.423"
                        className="middle"
                        animate={isOpen ? { opacity: 0 } : { opacity: 1 }}
                        transition={{ duration: 0.1 }}
                    />
                    <Path
                        d="M 2 16.346 L 20 16.346"
                        className="bottom"
                        animate={isOpen ? variants.open : variants.closed}
                        transition={{ duration: 0.1 }}
                        variants={variants}
                        initial="closed"
                    />
                </svg>
            </button>
        </div>
    );
};

export default MenuToggle;
