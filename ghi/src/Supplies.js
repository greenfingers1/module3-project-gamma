import React, { useState, useEffect } from "react";
import './Construct.css';
import './Catalog.css';
import { Link } from 'react-router-dom';


function ListSupplies() {
    const [supplies, setSupplies] = useState([]);
    const fetchData = async () => {
        const response = await fetch("https://mar-11-pt-greenfingers1.mod3projects.com/catalog/supplies");
        if (response.ok) {
            const supplies = await response.json();
            setSupplies(supplies); //Could be wrong about the dot operator here since diff file name and query name.
        } else {
            console.error(response);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="App">
            <header className="App-header">
                <div className="top-bar">
                    <ul>
                        <li><Link to="/home">Home Page</Link></li>
                        <li><Link to="/">Blog</Link></li>
                        <li><Link to="/catalog">Plant Shop</Link></li>
                        <li><Link to="/">Blog</Link></li>
                        <li><Link to="/catalog/supplies">Supplies</Link></li>
                        <li><Link to="/cart">Cart</Link></li>
                    </ul>
                </div>
            </header>

            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Image</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    {supplies.map(supply => (
                        <tr key={supply.id}>
                            <td>{supply.name}</td>
                            <td>{supply.price}</td>
                            <td>{supply.image}</td>
                            <td>{supply.description}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
            <div>
                <Link to="/catalog/supplies/create">
                    <button>Add Supply</button>
                </Link>
            </div>
        </div>
    );

}

export default ListSupplies;
