import React, { useState, useEffect } from 'react';
import axios from 'axios';
import styles from './CompetitionsList.module.css';
import DropDownMenu from './DropDownMenu'

const CompetitionsList = () => {
    const [competitionIds, setCompetitionIds] = useState([]);
    const startYear = 2023;
    const seasons = ['Summer', 'Autumn', 'Winter', 'Spring'];

    useEffect(() => {
        fetchCompetitionIds();
    }, []);

    const fetchCompetitionIds = async () => {
        try {
            const response = await axios.get('https://mar-11-pt-greenfingers1.mod3projects.com/competitions');
            setCompetitionIds(response.data);
        } catch (error) {
            console.error('Error fetching competition IDs:', error);
        }
    };

    const getCompetitionYearSeason = (competitionId) => {
        const yearIncrease = Math.floor((competitionId - 1) / 4);
        const seasonIndex = (competitionId - 1) % 4;
        return `${startYear + yearIncrease} ${seasons[seasonIndex]}`;
    };

    return (
        <div className={styles.container}>
            <DropDownMenu />
            <h2 className={styles.heading}>Competitions</h2>
            {competitionIds.length === 0 ? (
                <p>No competition IDs available.</p>
            ) : (
                <ul className={styles.competitionsList}>
                    {competitionIds.sort((a, b) => b - a).map((competitionId) => (
                        <li key={competitionId} className={styles.competitionItem}>
                            <a href={`https://greenfingers1.gitlab.io/module3-project-gamma/competitions/${competitionId}/posts`} className={styles.competitionLink}>
                                {getCompetitionYearSeason(competitionId)}
                            </a>
                        </li>
                    ))}
                </ul>
            )}
        </div>
    );
};

export default CompetitionsList;
