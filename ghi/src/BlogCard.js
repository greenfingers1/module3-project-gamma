import React from 'react';
import { Link } from 'react-router-dom';
import { motion } from 'framer-motion';
import { useInView } from 'react-intersection-observer';

const BlogCard = ({ blog }) => {
  const { ref, inView } = useInView({
    triggerOnce: false,
  });

  const variants = {
    hidden: {
      scale: 0.3,
      opacity: 0,
      filter: "blur(20px)"
    },
    show: {
      scale: 1,
      opacity: 1,
      filter: "blur(0px)",
      transition: {
        type: 'spring',
        duration: 0.5
      },
    },
  };

  return (
    <motion.div
      ref={ref}
      variants={variants}
      initial="hidden"
      animate={inView ? 'show' : 'hidden'}
      className="w-full md:w-1/2 lg:w-1/3 xl:w-1/4 p-4"
    >
      <div className="flex flex-col bg-white shadow-lg rounded-lg max-w-md mx-auto">
        <div className="flex-shrink-0">
          {blog.image && (
            <img className="h-48 w-full object-cover" src={blog.image} alt={blog.title} loading="lazy" />
          )}
        </div>
        <div className="px-6 py-4">
          <div className="font-bold text-xl mb-2 bg-gray-50">{blog.title}</div>
          <p className="text-gray-700 text-base">
            {blog.body.substring(0, 100)}...
          </p>
        </div>
        <div className="px-6 py-4 flex items-center justify-between">
          <Link to={`/blog/${blog.id}`} className="text-blue-500 hover:text-blue-700">
            Read More
          </Link>
        </div>
      </div>
    </motion.div>
  );
};

export default BlogCard;
